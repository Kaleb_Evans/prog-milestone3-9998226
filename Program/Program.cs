﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Program
{
    class Program
    {       
        static void Main(string[] args)
        {
          
            Console.WriteLine("Hello please select a pizza size.");
            Console.WriteLine();
            Console.WriteLine("Input the first capital letter of your desired size.");
            Console.WriteLine();
            Console.WriteLine("(S)mall");
            Console.WriteLine("(R)egular");
            Console.WriteLine("(L)arge");
            Console.WriteLine();

            string pizza_size = Console.ReadLine();

            decimal pizza_price;
            decimal topping_price;

            decimal current_order = 0.00M;

             
            if (pizza_size == "S")
            {
                pizza_size = "S";
                pizza_price = 5.00M;
                current_order += 5.00M;
            }

            else if (pizza_size == "R")
            {
                pizza_size = "R";
                pizza_price = 8.50M;
                current_order += 8.50M;
            }

            else if (pizza_size == "L")
            {
                pizza_size = "L";
                pizza_price = 13.00M;
                current_order += 13.00M;
            }

            else
            {
                Console.Clear();
                Console.WriteLine("Please follow the correct format (S, R, L)");
              
            }
      
            Console.ReadKey();

            Console.Clear();

            Console.WriteLine();
            Console.WriteLine("Please choose a pizza topping");
            Console.WriteLine();
            Console.WriteLine("(M)eat Lovers");
            Console.WriteLine("(C)heese");
            Console.WriteLine("(H)aweiian");
            Console.WriteLine();

            string topping_type = Console.ReadLine();

            if (topping_type == "M")
            {
                topping_price = 1.00M;
                current_order += 1.00M;
            }

            else if (topping_type == "C")
            {
                topping_price = 0.50M;
                current_order += 0.50M;
            }

            else if (topping_type == "H")
            {
                topping_price = 1.50M;
                current_order += 1.50M;
            }

            else 
            {
                topping_price = 0;
                current_order += 0.00M;
            }

            Console.ReadKey();

            Console.Clear();

            Console.WriteLine();
            Console.WriteLine($"You've ordered a {pizza_size} pizza with {topping_type} toppings.");
            Console.WriteLine();
            Console.WriteLine($"Your final total is: ${current_order}.");
            Console.WriteLine();


        }
    }
}
